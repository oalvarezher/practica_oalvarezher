import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ToastModule } from 'primeng/toast';
import { MenuComponent } from './menu/menu.component';
import { ListBirdsComponent } from './list-birds/list-birds.component';
import { ItemBirdComponent } from './item-bird/item-bird.component';
import { DetailBirdComponent } from './detail-bird/detail-bird.component';
import { TableModule } from 'primeng/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OrnitologiaService } from './ornitologia.service';
import { AddSightingComponent } from './add-sighting/add-sighting.component';
import { AddBirdComponent } from './add-bird/add-bird.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    MenuComponent,
    ListBirdsComponent,
    ItemBirdComponent,
    DetailBirdComponent,
    AddSightingComponent,
    AddBirdComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastModule,
    TableModule
  ],
  providers: [OrnitologiaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
