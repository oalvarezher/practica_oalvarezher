import { TestBed } from '@angular/core/testing';

import { OrnitologiaService } from './ornitologia.service';

describe('OrnitologiaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrnitologiaService = TestBed.get(OrnitologiaService);
    expect(service).toBeTruthy();
  });
});
