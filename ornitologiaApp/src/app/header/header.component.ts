import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  /* Nos indica si debe estar el botón de volver */
  isBackButton = false;

  /* Variable para deshabilitar el botón del menú cuando estemos en la pantalla del mismo */
  disableButton = false;

  /* Caso especial en la pantalla de detalle cuando volvemos de añadir un avistamiento */
  backToList: boolean;

  constructor(
    public authenticationService: AuthenticationService,
    public router: Router,
    private location: Location,
  ) { }

  ngOnInit() {
    if (this.router.url === '/menu') {
      this.disableButton = true;
    } else if (this.router.url === '/list-birds') {
      this.isBackButton = false;
    } else if (this.router.url.includes('detail')) {
      this.backToList = true;
      this.isBackButton = true;
    } else {
      this.isBackButton = true;
    }
  }

  /* Permite navegar a la pantalla del menú */
  goToMenu() {
    this.router.navigate(['menu']);
  }

  /* Pantalla que permite volver a la pantalla anterior mediante la propiedad location de angular. En caso de que
  backToList sea true debe navegar al listado de pájaros ya que si se navega cuando añadimos un avistamiento, el location
  ya no permite volver a la pantalla del listado sino de añadir avistamiento */
  back() {
    if (this.backToList) {
      this.router.navigate(['list-birds']);
    } else {
      this.location.back();
    }
  }

}
