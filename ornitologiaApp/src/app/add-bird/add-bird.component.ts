import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AddBird, OrnitologiaService } from '../ornitologia.service';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { GeolocalizationService } from '../geolocalization.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-bird',
  templateUrl: './add-bird.component.html',
  styleUrls: ['./add-bird.component.scss'],
  providers: [MessageService]
})
export class AddBirdComponent implements OnInit, OnDestroy {

  /* Formulario para añadir una nueva ave */
  addBirdForm: FormGroup;

  addBird: AddBird;

  showGeo = false;

  preloader: boolean;

  subscription: Subscription;

  /* Boolean que indica si se esta obteniendo la geolocalización */
  getGeo: boolean;

  constructor(
    private ornitologiaService: OrnitologiaService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private messageService: MessageService,
    private geolocalizationService: GeolocalizationService,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  /* Construye el formulario para añadir un ave */
  buildForm() {
    this.addBirdForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required, Validators.minLength(20)]),
      place: new FormControl({ value: '', disabled: true }, []),
      lat: new FormControl({ value: '', disabled: true }, []),
      long: new FormControl({ value: '', disabled: true }, [])
    });
  }

  /* Guarda la ave enviandola a la API */
  saveNewAddBird() {
    this.addBird = new AddBird();
    this.addBird.idUser = this.authenticationService.getId();
    this.addBird.bird_name = this.addBirdForm.controls.name.value;
    this.addBird.bird_description = this.addBirdForm.controls.description.value;
    this.addBird.place = this.addBirdForm.controls.place.value;
    this.addBird.long = this.addBirdForm.controls.long.value;
    this.addBird.lat = this.addBirdForm.controls.lat.value;
    Promise.resolve().then(() => this.preloader = true);
    this.ornitologiaService.addBird(this.addBird).subscribe(
      response => {
        /* Se añade un delay con el fin de permitir mostrar el preloader ya que la llamada se hace demasiado rápida */
        setTimeout(() => {
          this.preloader = false;
          if (response.status === 'OK') {
            this.messageService.add({ severity: 'success', summary: 'Éxito', detail: 'Se ha añadido una nueva ave' });
          }
          if (response.status === 'KO') {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'response.message' });
          }
        }, 500);
      },
      () => {
        this.preloader = false;
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se ha podido añadir un ave' });
      }
    );
  }

  /* Evento que se lanza cuando marcamos o desmarcamos el checkbox */
  handleSelected(eve: any) {
    if (eve) {
      this.showGeo = true;
      this.addBirdForm.get('place').setValidators(Validators.required);
      this.addBirdForm.get('place').updateValueAndValidity();
      this.addBirdForm.get('lat').setValidators(Validators.required);
      this.addBirdForm.get('lat').updateValueAndValidity();
      this.addBirdForm.get('long').setValidators(Validators.required);
      this.addBirdForm.get('long').updateValueAndValidity();
      this.getLocation();
    } else {
      this.showGeo = false;
      this.addBirdForm.get('place').reset();
      this.addBirdForm.get('place').clearValidators();
      this.addBirdForm.get('place').updateValueAndValidity();
      this.addBirdForm.get('lat').reset();
      this.addBirdForm.get('lat').clearValidators();
      this.addBirdForm.get('lat').updateValueAndValidity();
      this.addBirdForm.get('long').reset();
      this.addBirdForm.get('long').clearValidators();
      this.addBirdForm.get('long').updateValueAndValidity();
    }
  }

  /* Obtenemos la geolocalización */
  getLocation() {
    this.geolocalizationService.getLocation();
    Promise.resolve().then(() => this.getGeo = true);
    this.subscription = this.geolocalizationService.getGeo().subscribe(geo => {
      this.getGeo = false;
      this.addBirdForm.get('lat').setValue(geo.geolocalization.lat);
      this.addBirdForm.get('long').setValue(geo.geolocalization.long);
      this.addBirdForm.get('place').setValue(geo.geolocalization.place);
    });
  }

  /* Navegamos a la pantalla del listado de aves cuando se cierra el toast */
  navigate() {
    this.router.navigate(['list-birds']);
  }

}
