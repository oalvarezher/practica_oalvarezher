import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private route: Router,
  ) { }

  ngOnInit() {
  }

  /* Navega a la pantalla del listado de aves */
  goToListBirds() {
    this.route.navigate(['list-birds']);
  }

  /* Desconecta al usuario y navega al login */
  logout() {
    this.authenticationService.logout();
    this.route.navigate(['login']);
  }

  /* Navega a la pantalla de añadir un ave */
  addBird() {
    this.route.navigate(['add-bird']);
  }

}
