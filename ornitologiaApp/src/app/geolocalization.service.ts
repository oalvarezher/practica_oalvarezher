import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
declare let L;

@Injectable({
  providedIn: 'root'
})
export class GeolocalizationService {

  constructor() { }

  private subject = new Subject<any>();

  /* Obtiene la geolocalización mediante la API html5 y leaflet.js */
  getLocation() {
    let geo = new Geo();
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          geo.lat = position.coords.latitude.toString();
          geo.long = position.coords.longitude.toString();
          const latlng = new L.LatLng(position.coords.latitude, position.coords.longitude);
          const geocodeService = L.esri.Geocoding.geocodeService();
          geocodeService.reverse()
            .latlng(latlng)
            .run((error, result, response) => {
              geo.place = result.address.Address;
              this.subject.next({ geolocalization: geo });
            });
        },
        () => {
          geo.lat = '0';
          geo.long = '0';
          geo.place = 'Desconocido';
          this.subject.next({ geolocalization: geo });
        }
      );
    } else {
      geo.lat = '0';
      geo.long = '0';
      geo.place = 'Desconocido';
      this.subject.next({ geolocalization: geo });
    }
  }

  /* Devuelve la susbcripción para poder susbcribirse */
  getGeo(): Observable<any> {
    return this.subject.asObservable();
  }

}

export class Geo {
  place: string;
  lat: string;
  long: string;
}
