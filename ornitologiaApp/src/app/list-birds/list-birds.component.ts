import { Component, OnInit } from '@angular/core';
import { OrnitologiaService, Bird } from '../ornitologia.service';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-list-birds',
  templateUrl: './list-birds.component.html',
  styleUrls: ['./list-birds.component.scss'],
  providers: [MessageService]
})
export class ListBirdsComponent implements OnInit {

  birds: Bird[] = [];

  constructor(
    private ornitologiaService: OrnitologiaService,
    private authenticationService: AuthenticationService,
    private messageService: MessageService,
    private router: Router,
  ) { }

  ngOnInit() {
    /* Llama a la api para recuperar el listado de aves */
    this.ornitologiaService.getBirds(this.authenticationService.getId()).subscribe(
      birds => {
        /* Se añade un delay con el fin de permitir mostrar el preloader ya que la llamada se hace demasiado rápida */
        setTimeout(() => {
          this.birds = birds;
        }, 500);
      },
      error => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se ha podido recuperar el listado de aves' });
      }
    );
  }

  /* Navega a la pantalla de añadir un ave */
  addBird() {
    this.router.navigate(['add-bird']);
  }

}
