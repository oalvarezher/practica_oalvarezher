import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailBirdComponent } from './detail-bird.component';

describe('DetailBirdComponent', () => {
  let component: DetailBirdComponent;
  let fixture: ComponentFixture<DetailBirdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailBirdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailBirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
