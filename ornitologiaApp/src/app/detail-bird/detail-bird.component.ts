import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrnitologiaService, BirdDetail, Sighting } from '../ornitologia.service';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-detail-bird',
  templateUrl: './detail-bird.component.html',
  styleUrls: ['./detail-bird.component.scss'],
  providers: [MessageService]
})
export class DetailBirdComponent implements OnInit {

  /* Ave que se va a editar */
  bird: BirdDetail;

  /* Avistamientos que se muestran en la tabla */
  sightings: Sighting[] = [];

  /* Columnas de la tabla */
  cols: any[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private ornitologiaService: OrnitologiaService,
    private messageService: MessageService,
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.getBird(params['id']);
    });
    this.cols = [
      { field: 'id', header: 'Id' },
      { field: 'idAve', header: 'Id del ave' },
      { field: 'place', header: 'Lugar' },
      { field: 'long', header: 'Longitud' },
      { field: 'lat', header: 'Latitud' }
    ];
  }

  /* Obtiene el ave a través del id llamando al servicio */
  getBird(id: string) {
    this.ornitologiaService.getBird(id).subscribe(
      bird => {
        /* Se añade un delay con el fin de permitir mostrar el preloader ya que la llamada se hace demasiado rápida */
        setTimeout(() => {
          this.bird = bird;
          this.sightings = bird.sightings_list;
        }, 500);
      },
      () => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se ha podido recuperar el ave' });
      }
    );
  }

  /*Navegar a pantalla de añadir avistamiento */
  addSighting() {
    this.route.navigate(['add-sighting/' + this.bird.id]);
  }

}
