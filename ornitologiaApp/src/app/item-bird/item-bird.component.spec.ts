import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBirdComponent } from './item-bird.component';

describe('ItemBirdComponent', () => {
  let component: ItemBirdComponent;
  let fixture: ComponentFixture<ItemBirdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemBirdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemBirdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
