import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-item-bird',
  templateUrl: './item-bird.component.html',
  styleUrls: ['./item-bird.component.scss']
})
export class ItemBirdComponent implements OnInit {

  @Input() id: number;

  @Input() image: number;

  @Input() name: string;

  @Input() sightings: string;

  @Input() mine: number;

  constructor(
    private route: Router,
  ) { }

  ngOnInit() {
  }

  /* Navega a la pantalla de detalle del ave */
  goToDetail() {
    this.route.navigate(['detail-bird/' + this.id]);
  }

}
