import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor() { }

  saveId(id: string) {
    localStorage.setItem('id_ornitologiaApp', id);
  }

  getId() {
    return localStorage.getItem('id_ornitologiaApp');
  }

  isLogged() {
    return localStorage.getItem('id_ornitologiaApp') !== null;
  }

  logout() {
    localStorage.removeItem('id_ornitologiaApp');
  }

}
