import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sighting, OrnitologiaService } from '../ornitologia.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { GeolocalizationService, Geo } from '../geolocalization.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-sighting',
  templateUrl: './add-sighting.component.html',
  styleUrls: ['./add-sighting.component.scss'],
  providers: [MessageService]
})
export class AddSightingComponent implements OnInit, OnDestroy {

  /* Formulario para añadir un nuevo avistamiento */
  sightingForm: FormGroup;

  /* Objeto avistamiento */
  sighting: Sighting;

  preloader: boolean;

  /* Boolean que indica si se esta obteniendo la geolocalización */
  getGeo: boolean;

  subscription: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private ornitologiaService: OrnitologiaService,
    private router: Router,
    private messageService: MessageService,
    private geolocalizationService: GeolocalizationService,
  ) { }

  ngOnInit() {
    /* Obtenemos el id de la ruta */
    this.activatedRoute.params.subscribe(params => {
      this.sighting = new Sighting();
      this.sighting.idAve = params['id'];
      this.buildForm();
      this.getLocation();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  /* Construye el formulario para añadir un avistamiento */
  buildForm() {
    this.sightingForm = new FormGroup({
      place: new FormControl({ value: '', disabled: true }, [Validators.required]),
      lat: new FormControl({ value: '', disabled: true }, [Validators.required]),
      long: new FormControl({ value: '', disabled: true }, [Validators.required])
    });
  }

  /* Obtiene la geolocalización. En caso contrario coloca un 0 */
  getLocation() {
    this.geolocalizationService.getLocation();
    Promise.resolve().then(() => this.getGeo = true);
    this.subscription = this.geolocalizationService.getGeo().subscribe(geo => {
      this.getGeo = false;
      this.sightingForm.get('lat').setValue(geo.geolocalization.lat);
      this.sightingForm.get('long').setValue(geo.geolocalization.long);
      this.sightingForm.get('place').setValue(geo.geolocalization.place);
    });
  }

  /* Guarda el avistamiento y lo envía a la api */
  saveNewSighting() {
    this.sighting.place = this.sightingForm.controls.place.value;
    this.sighting.lat = this.sightingForm.controls.lat.value;
    this.sighting.long = this.sightingForm.controls.long.value;
    Promise.resolve().then(() => this.preloader = true);
    this.ornitologiaService.addSighting(this.sighting).subscribe(
      response => {
        /* Se añade un delay con el fin de permitir mostrar el preloader ya que la llamada se hace demasiado rápida */
        setTimeout(() => {
          this.preloader = false;
          if (response.status === 'OK') {
            this.messageService.add({ severity: 'success', summary: 'Éxito', detail: 'Se ha añadido un nuevo avistamiento' });
          }
          if (response.status === 'KO') {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: response.message });
          }
        }, 500);
      },
      () => {
        this.preloader = false;
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'No se ha podido añadir un avistamiento' });
      }
    );
  }

  /* Navega a la pantalla de detalle cuando el evento de cierre del toast se produce */
  navigate() {
    this.router.navigate(['detail-bird/', this.sighting.idAve]);
  }

}
