import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OrnitologiaService {

  private basePath = 'http://dev.contanimacion.com/birds/public/';
  private loginUrl: string;
  private getBirdsUrl: string;
  private getBirdUrl: string;
  private addSightingUrl: string;
  private addBirdUrl: string;

  constructor(
    private http: HttpClient,
  ) {
    this.loginUrl = this.basePath + 'login/';
    this.getBirdsUrl = this.basePath + 'getBirds/';
    this.getBirdUrl = this.basePath + 'getBirdDetails/';
    this.addSightingUrl = this.basePath + 'addSighting/';
    this.addBirdUrl = this.basePath + 'addBird/';
  }

  login(user?: string, password?: string): Observable<Login> {
    let params = new HttpParams();
    params = params.append('user', user);
    params = params.append('password', password);
    return this.http.post<Login>(this.loginUrl, null, { params });
  }

  getBirds(id?: string): Observable<Bird[]> {
    return this.http.get<Bird[]>(this.getBirdsUrl + id);
  }

  getBird(id?: string): Observable<BirdDetail> {
    return this.http.get<BirdDetail>(this.getBirdUrl + id).pipe(
      map(data => data[0])
    );
  }

  addSighting(sighting: Sighting): Observable<Response> {
    return this.http.post<Response>(this.addSightingUrl, sighting);
  }

  addBird(addBird: AddBird): Observable<Response> {
    return this.http.post<Response>(this.addBirdUrl, addBird);
  }

}

export class Login {
  status: string;
  id: string;
  message: string;
}

export class Bird {
  id: string;
  bird_name: string;
  bird_image: string;
  bird_sightings: string;
  mine: number;
}

export class BirdDetail extends Bird {
  idUser: string;
  bird_description: string;
  sightings_list: Sighting[] = [];
}

export class Sighting {
  id: string;
  idAve: string;
  place: string;
  long: string;
  lat: string;
}

export class Response {
  status: string;
  message: string;
}

export class AddBird {
  idUser: string;
  bird_name: string;
  bird_description: string;
  place: string;
  long: string;
  lat: string;
}
