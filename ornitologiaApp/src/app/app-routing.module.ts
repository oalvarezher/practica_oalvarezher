import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { AuthenticationGuard } from './authentication.guard';
import { LoginGuard } from './login.guard';
import { ListBirdsComponent } from './list-birds/list-birds.component';
import { DetailBirdComponent } from './detail-bird/detail-bird.component';
import { AddSightingComponent } from './add-sighting/add-sighting.component';
import { AddBirdComponent } from './add-bird/add-bird.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [LoginGuard]
  },
  {
    path: 'menu',
    component: MenuComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'list-birds',
    component: ListBirdsComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'detail-bird/:id',
    component: DetailBirdComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'add-sighting/:id',
    component: AddSightingComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'add-bird',
    component: AddBirdComponent,
    canActivate: [AuthenticationGuard]
  },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
