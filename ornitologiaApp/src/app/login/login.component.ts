import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { OrnitologiaService } from '../ornitologia.service';
import { AuthenticationService } from '../authentication.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  preloader: boolean;

  constructor(
    private ornitologiaService: OrnitologiaService,
    private authenticationService: AuthenticationService,
    private messageService: MessageService,
    private route: Router,
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  /* Construye el formulario de login */
  buildForm() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
  }

  /* Llama al servicio encargado de realizar el login */
  login() {
    Promise.resolve().then(() => this.preloader = true);
    this.ornitologiaService.login(this.loginForm.value.email, this.loginForm.value.password).subscribe(
      login => {
        /* Se añade un delay con el fin de permitir mostrar el preloader ya que la llamada se hace demasiado rápida */
        setTimeout(() => {
          this.preloader = false;
          if (login.status === 'OK') {
            this.authenticationService.saveId(login.id);
            this.route.navigate(['menu']);
          } 
          if (login.status === 'KO') {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: login.message });
          }
        }, 500);
      },
      error => {
        this.preloader = false;
        this.messageService.add({ severity: 'error', summary: 'Error', detail: error.message });
      }
    );
  }

}
